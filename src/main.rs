extern crate co3326cw18;
extern crate serde_json;
extern crate num;
extern crate rand;

use num::bigint::{BigInt, ToBigInt};
use num::integer::Integer;
use rand::Rng;

fn parse_example(data: String) -> Result<(serde_json::Value), serde_json::Error> {
    // Some JSON input data as a &str.

    // Parse the string of data into serde_json::Value.
    let v: serde_json::Value = serde_json::from_str(&data)?;

    // Access parts of the data by indexing with square brackets.
    println!("Name: {}, composite {}", v["name"], v["alice"]["modulus"]);

    Ok((v))
}

// // https://brilliant.org/wiki/eulers-criterion/
// fn legendre_symbol(n: BigInt, p: BigInt) {
//     if (n % p == 0) {
//         return 0;
//     }
//     let mut e = (p-1) / 2;
//     //
// }

// fn gen_factor_base(num : BigInt, b: u32) {
//     let mut factor_base : Vec<u32> = Vec::new();
//     let mut sieve : Vec<bool> = vec![false; b as usize];
//     let mut p : u32 = 2;
//     while p <= b {
//         if sieve[p as usize] {
//             continue;
//         }
//         p += 2;
//     }
// }

fn quadratic_sieve(num : BigInt) {

}

fn square_root(num : BigInt) {
    let mut y : BigInt;
    //for (y = num.div_floor(&2); )
}

fn abs_bigint(num: BigInt) -> BigInt {
    return if num >= 0.to_bigint().unwrap() {num} else {-num};
}

fn gcd_bigint(a: &BigInt, b: &BigInt) -> BigInt {
    if b == &(0.to_bigint().unwrap()) {
        return a.to_bigint().unwrap();
    } else {
        return gcd_bigint(&b, &(a % b));
    }
}

fn pollard_rho(num: BigInt) {
    let n = num.clone();
    let mut rng = rand::thread_rng();
    /*
    * x is tortoise, y is hare
    */
    let mut x = rng.gen::<u64>().to_bigint().unwrap();
    let mut y = x.clone();
    let c = rng.gen::<u64>().to_bigint().unwrap();
    let mut g = 1.to_bigint().unwrap();
    while g == 1.to_bigint().unwrap() {
        x = ((&x * &x) % &n + &c) % &n;
        y = ((&y * &y) % &n + &c) % &n;
        y = ((&y * &y) % &n + &c) % &n;
        let diff = abs_bigint(&x - &y);
        g = gcd_bigint(&diff, &num);
    }
    println!("{}", g);
}

fn naive_factorise(num : BigInt) {
    //let mut found = false;
    for i in 2..i32::max_value() {
        //let convert = i.to_bigint()
        // this print function purely for time tracking
        if i % 1000000 == 0 {
            println!("{}", i);
        }
        let convert = i.to_bigint().unwrap();
        if (num.is_multiple_of(&convert)) {
            println!("{}", i);
            break;
        }
    }
}

fn main() {
    let mut input = String::new();
    /*
    match std::io::stdin().read_line(&mut input) {
        Ok(_) => {
            //println!("{} bytes read", n);
            input = input.trim().to_string();
            let raw_data = co3326cw18::read_file(input);
            parse_example(raw_data);
            //println!("{:?}", 14.to_bigint());
            //let _s = ("86415339751486246917993580247").parse::<BigInt>().unwrap();
            let _s = ("481584764130312863").parse::<BigInt>().unwrap();
            // naive_factorise(_s.to_bigint().unwrap());
            pollard_rho(_s.to_bigint().unwrap());
            //naive_factorise(20920733.to_bigint().unwrap());
        }
        Err(error) => println!("error: {}", error),
    }
    */
    let _n = ("481584764130312863").parse::<BigInt>().unwrap();
    let _p = ("481584764130312863").parse::<BigInt>().unwrap();
    //legendre_symbol(_n, _p);
}
